<?php

namespace App\Controller;

use App\Entity\Ofertes;
use App\Entity\Categoria;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class EmpresaController extends AbstractController
{

    /**
     * Función la cual crea el formulario para las empresas
     *
     * @Route("/insertarOferta", name="insertarOferta")
     */

    public function insertOfertesForm(Request $request)
    {
        $oferta = new Ofertes();

        $oferta->setData(new \DateTime("now"));
        $oferta->setValidada('2');

        $form = $this->createFormBuilder($oferta)
            ->add('Titol', TextType::class, array('attr' => array('class' => 'col-md-6 form-group bmd-label-floating form-control')))
            ->add('Descripcio', TextareaType::class, array('attr' => array('class' => 'col-md-12 form-group bmd-label-floating form-control')))
            ->add('Ubicacio', TextType::class, array('attr' => array('class' => 'col-md-6 form-group bmd-label-floating form-control')))
            ->add('Email', EmailType::class, array('attr' => array('class' => 'col-md-6 form-group bmd-label-floating form-control'))) //Indicar en la vista que el mail no es público y que es donde se enviará el CV del usuario inscrito
            ->add('Categoria', EntityType::class, [
                'class' => 'App\Entity\Categoria',
                'choice_label' => 'descripcio'
            ])
            ->add('Inserta', SubmitType::class, ['label' => 'Guardar Oferta'])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {


            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($oferta);
            $entityManager->flush();

            //return new Response('Oferta ' . $oferta->getTitol() . ' insertado en BD');
            return $this->redirectToRoute('exitoInsert');
        }

        return $this->render('Empreses/formEmpresas.html.twig', [
            'form' => $form->createView(),
            "pagina" => "empreses"
        ]);
    }

    /**
     * @Route("/ofertaInsertada", name="exitoInsert")
     */
    public function exitoInsert()
    {
        return $this->render('Empreses/ofertaInsertada.html.twig',
            array("pagina" => "empreses",
                "insertUpdate" => "insert"
            )
        );
    }

}
