<?php

namespace App\Controller;

use App\Entity\Candidat;
use App\Entity\CandidatOferta;
use App\Entity\Ofertes;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\pujarcv;


class UsuariController extends AbstractController
{

    /**
     * Función que retorna las ofertas validadas de los ultimos 3 meses
     *
     * @Route("/listarOfertasUsuario", name="listarOfertasUsuario", methods={"GET", "HEAD"})
     */
    public function mostrarOfertas()
    {
        $fecha_actual = new \DateTime("now");
        $fecha_actual->modify('-3 months');

        $repository = $this->getDoctrine()->getRepository(Ofertes::class);

        // query for a single Product by its primary key (usually "id")
        $ofertas = $repository->createQueryBuilder('oferta')
            ->innerJoin('oferta.categoria', 'categoria')
            ->addSelect('categoria')
            ->where('oferta.categoria_id = :id')
            ->where('oferta.validada = 1')//condicion de la consulta a oferta
            ->andWhere('oferta.data >= :fecha3meses')//para obtener las ofertas de los ultimos 15 dias
            ->setParameter(':fecha3meses', $fecha_actual)
            ->orderBy('oferta.data', 'DESC')
            ->getQuery()
            ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);

        return new JsonResponse($ofertas);
    }

    /**
     * Función que retorna las inscripciones de un usuario en concreto
     *
     * @Route("/listarInscritasUsuario/{id}", name="listarInscritasUsuario", methods={"GET", "HEAD"})
     */
    public function mostrarInscripcions($id)
    {
        $repository = $this->getDoctrine()->getRepository(Candidat::class);

        $candidat = $repository->createQueryBuilder('candidat')
            ->addSelect('candidat')
            ->where('candidat.idGoogle = :idGoogle')//condicion de la consulta a oferta
            ->setParameter('idGoogle', $id)
            ->getQuery()
            ->getResult();

        $repository = $this->getDoctrine()->getRepository(CandidatOferta::class);

        // query for a single Product by its primary key (usually "id")
        $inscripcions = $repository->createQueryBuilder('inscripcions')
            ->innerJoin('inscripcions.candidat', 'idCandidato')
            ->innerJoin('inscripcions.ofertes', 'idOferta')
            ->addSelect('idCandidato', 'idOferta')
            ->where('inscripcions.candidat = :idUsuario')//condicion de la consulta a oferta
            ->setParameter('idUsuario', $candidat[0]->getId())
            ->getQuery()
            ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);

        return new JsonResponse($inscripcions);
    }

    /**
     * Función que retorna las ofertas ordenadas descendientemente,
     * validadas de los ultimos 15 días
     *
     * @Route("/listarOfertasOrdenadasUsuario", name="listarOfertasOrdenadasUsuario", methods={"GET", "HEAD"})
     */
    public function mostrarOfertasOrdenadas()
    {
        $repository = $this->getDoctrine()->getRepository(Ofertes::class);

        // query for a single Product by its primary key (usually "id")
        $ofertas = $repository->createQueryBuilder('oferta')
            ->innerJoin('oferta.categoria', 'categoria')
            ->addSelect('categoria')
            ->where('oferta.categoria_id = :id')
            ->where('oferta.validada = 1')//condicion de la consulta a oferta
            ->andWhere('oferta.data >= CURRENT_DATE() - 15')//para obtener las ofertas de los ultimos 15 dias
            ->orderBy('oferta.data', 'DESC')
            ->getQuery()
            ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);

        return new JsonResponse($ofertas);
    }

    /**
     * Función que retorna las ofertas ordenadas de una categoria en concreto
     *
     * @Route("/listarOfertasCategoriaUsuario/{categoria}", name="listarOfertasCategoriaUsuario", methods={"GET", "HEAD"})
     */
    public function mostrarOfertasCategoria($categoria)
    {
        $repository = $this->getDoctrine()->getRepository(Ofertes::class);

        // query for a single Product by its primary key (usually "id")
        $ofertas = $repository->createQueryBuilder('oferta')
            ->innerJoin('oferta.categoria', 'categoria')
            ->addSelect('categoria')
            ->where('oferta.categoria_id = :id')
            ->where('oferta.validada = 1')//condicion de la consulta a oferta
            ->andWhere('categoria.descripcio = :nomCategoria')
            ->setParameter('nomCategoria', $categoria)
            ->orderBy('oferta.data', 'DESC')
            ->getQuery()
            ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);

        return new JsonResponse($ofertas);
    }


    /**
     * Función que inscribe a un usuario en una oferta,
     * envia mail a la empresa conforma un usuario se ha inscrito
     *
     * @Route("/inscribirOferta", name="inscribirOferta", methods={"POST"})
     */
    public function inscribirOferta(Request $request, \Swift_Mailer $mailer)
    {
        $data = json_decode($request->getContent(), true);

        $repository = $this->getDoctrine()->getRepository(Ofertes::class);

        $ofertas = $repository->createQueryBuilder('oferta')
            ->addSelect('oferta')
            ->where('oferta.id = :idOferta')//condicion de la consulta a oferta
            ->setParameter('idOferta', $data['idOferta'])
            ->getQuery()
            ->getResult();

        $repository = $this->getDoctrine()->getRepository(Candidat::class);

        $candidat = $repository->createQueryBuilder('candidat')
            ->addSelect('candidat')
            ->where('candidat.idGoogle = :idGoogle')//condicion de la consulta a oferta
            ->setParameter('idGoogle', $data['idGoogle'])
            ->getQuery()
            ->getResult();

        $fecha_actual = new \DateTime("now");

        $candidatOferta = new CandidatOferta();
        $candidatOferta->setCandidat($candidat[0]);
        $candidatOferta->setOfertes($ofertas[0]);
        $candidatOferta->setHora($fecha_actual);

        $candidatOferta->setIp('192.168.205.221');

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($candidatOferta);
        $entityManager->flush();

        $linkCV = $candidat[0]->getLinkCV();

        //ENVIAR MAIL

        $message = (new \Swift_Message('Usuario Registrado'));

        $data['image_src'] = $message->embed(\Swift_Image::fromPath('img/pedral.png'));

        $message->setFrom('pedraljob@gmail.com')
            ->setTo($ofertas[0]->getEmail())
            ->setBody(
                $this->renderView(
                    'Email/registration.html.twig',
                    ['candidato' => $candidat[0],
                        'oferta' => $ofertas[0],
                        "imagen" => $data['image_src']
                    ]
                ),
                'text/html'
            )
            ->attach(\Swift_Attachment::fromPath("$linkCV"));

        $mailer->send($message);

        return $this->redirectToRoute("ofertasUsuario", ["idGoogle" => $candidat[0]->getIdGoogle()]);
    }

    /**
     * Función que comprueba el tipo de usuario
     * Si se corresponde al administrador o a un usuario normal
     *
     * @Route("/comprovarUsuarioPrueba/{username}/{email}/{idGoogle}", name="comprovarUsuarioPrueba")
     */
    public function comprovarUsuarioPrueba($username, $email, $idGoogle)
    {
        $repository = $this->getDoctrine()->getRepository(Candidat::class);

        $candidat = $repository->createQueryBuilder('candidat')
            ->where('candidat.idGoogle = :idGoogle')//condicion de la consulta a oferta
            ->setParameter(':idGoogle', $idGoogle)
            ->getQuery()
            ->getResult();

        if (!$candidat) {

            $candidat = new Candidat();
            $candidat->setUsername($username);
            $candidat->setCorreo($email);
            $candidat->setIdGoogle($idGoogle);
            $candidat->setLinkCV("null");

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($candidat);
            $entityManager->flush();
        }

        if (gettype($candidat) == "array") {
            $correoCandidat = $candidat[0]->getCorreo();
        } else $correoCandidat = $candidat->getCorreo();

        if ($correoCandidat == "pedraljob@gmail.com") {
            return $this->redirectToRoute('adminMenu');
        }
        return $this->redirectToRoute("ofertasUsuario", ["idGoogle" => $idGoogle]);
    }

    /**
     * Función que renderiza la pagina del usuario
     * y añade el formulario para subir el CV
     *
     * @Route("/ofertasUsuario/{idGoogle}", name="ofertasUsuario")
     */
    public function mostrarVistaOfertas(Request $request, $idGoogle)
    {
        $form = $this->createFormBuilder()
            ->add('link_cv', FileType::class, array(
                "label" => "Arxiu:",
                "attr" => array("class" => "Candidat")
            ))

            ->add('submit', SubmitType::class, array(
                'label' => 'Pujar curriculum',
                'attr' => array('class' => 'btn btn-primary pull-right col-md-6')
            ))

            ->getForm();


        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {

            $fecha = new \DateTime("now");

            $file = $form['link_cv']->getData();
            $extension = $file->guessExtension();
            $file_name = $idGoogle . "_" . $fecha->format('Y-m-d') . "_" . $fecha->format("H:i:s") . "." . $extension;

            $file->move("uploads", $file_name);

            //Buscamos el candidato con id de google pasado
            $repository = $this->getDoctrine()->getRepository(Candidat::class);

            $candidat = $repository->createQueryBuilder('candidat')
                ->where('candidat.idGoogle = :idGoogle')//condicion de la consulta a oferta
                ->setParameter(':idGoogle', $idGoogle)
                ->getQuery()
                ->getResult();

            $candidat[0]->setLinkCV("uploads/" . $file_name);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($candidat[0]);
            $entityManager->flush();

        }

        return $this->render('Usuario/usuarioOfertas.html.twig',
            array("pagina" => "usuari",
                'form' => $form->createView()
            )
        );
    }

}
