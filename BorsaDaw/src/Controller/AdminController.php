<?php

namespace App\Controller;

use App\Entity\Ofertes;
use App\Entity\Categoria;
use App\Entity\CandidatOferta;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\Query;

class AdminController extends AbstractController
{

    /**
     * Función que obtiene todas las ofertas ordenadas descendientemente
     * y las inscripciones de cada una de las ofertas
     *
     * @Route("/listarOfertasAdmin", name="listarOfertasAdmin")
     */

    public function mostrarTodasOfertas()
    {

        $ofertas = $this->getDoctrine()
            ->getRepository(Ofertes::class)
            ->findBy( array(), array('data' => 'DESC') );

        $numInscripciones = [];
        foreach ($ofertas as $oferta) {

            $inscripcions = $this->getDoctrine()
                ->getRepository(CandidatOferta::class)
                ->findBy(
                    ['ofertes' => $oferta->getId()]
                );
            array_push($numInscripciones, sizeof($inscripcions));

        }

        return $this->render('Administrador/panelAdmin.html.twig',
            array("ofertas" => $ofertas,
                "pendientes" => "no",
                "pagina" => "admin",
                "numInscripcionesOferta" => $numInscripciones)
        );
    }

    /**
     * Función que obtiene las ofertas pendientes por calificar
     *
     * @Route("/listarOfertasPendientesAdmin", name="listarOfertasPendientesAdmin")
     */

    public function mostrarOfertasPendientes()
    {
        $ofertas = $this->getDoctrine()
            ->getRepository(Ofertes::class)
            ->findByPendientes('2');

        return $this->render('Administrador/panelAdmin.html.twig',
            array("ofertas" => $ofertas,
                "pendientes" => "si",
                "pagina" => "admin")
        );
    }

    /**
     * @Route("/admin", name="adminMenu")
     */
    public function mostrarMenuAdmin()
    {
        return $this->render('Administrador/menuPanel.html.twig',
            array("pagina" => "admin")
        );
    }

    /**
     * Función con la cual aceptamos la oferta que anteriormente estaba pendiente
     *
     * @Route("/aceptar/{id}", name="aceptarOferta")
     */

    public function aceptarOferta($id){
        $entityManager = $this->getDoctrine()->getManager();
        $oferta = $entityManager->getRepository(Ofertes::class)->find($id);

        if (!$oferta) {
            throw $this->createNotFoundException(
                'No existeix la oferta amb id: '.$id
            );
        }

        $oferta->setValidada('1');
        $entityManager->flush();

        $ofertas = $this->getDoctrine()
            ->getRepository(Ofertes::class)
            ->findByPendientes('2');

        return $this->redirectToRoute('listarOfertasPendientesAdmin',
            array("ofertas" => $ofertas,
                "pendientes" => "si",
                "pagina" => "admin")
        );
    }


    /**
     * Función con la cual rechazamos la oferta que anteriormente estaba pendiente
     *
     * @Route("/rechazar/{id}", name="rechazarOferta")
     */

    public function rechazarOferta($id){
        $entityManager = $this->getDoctrine()->getManager();
        $oferta = $entityManager->getRepository(Ofertes::class)->find($id);

        if (!$oferta) {
            throw $this->createNotFoundException(
                'No existeix la oferta amb id: '.$id
            );
        }

        $oferta->setValidada('0');
        $entityManager->flush();


        $ofertas = $this->getDoctrine()
            ->getRepository(Ofertes::class)
            ->findByPendientes('2');

        return $this->redirectToRoute('listarOfertasPendientesAdmin',
            array("ofertas" => $ofertas,
                "pendientes" => "si",
                "pagina" => "admin")
        );
    }

    /**
     * Función con la cual eliminamos una oferta concreta
     *
     * @Route("/borrarOferta/{id}", name="borrarOferta")
     */
    public function borrarOferta($id, Request $request){
        $entityManager = $this->getDoctrine()->getManager();
        $oferta = $entityManager->getRepository(Ofertes::class)->find($id);

        if (!$oferta) {
            throw $this->createNotFoundException(
                'No existeix la oferta amb id: '.$id
            );
        }

        $entityManager->remove($oferta);
        $entityManager->flush();

        $ofertas = $this->getDoctrine()
            ->getRepository(Ofertes::class)
            ->findByPendientes('2');

        return $this->redirectToRoute('listarOfertasPendientesAdmin',
            array("ofertas" => $ofertas,
                "pendientes" => "si",
                "pagina" => "admin")
        );
    }

    /**
     * Función con la cual eliminamos una oferta concreta
     *
     * @Route("/borraOferta/{id}", name="borraOferta")
     */
    public function borraOferta($id, Request $request){
        $entityManager = $this->getDoctrine()->getManager();
        $oferta = $entityManager->getRepository(Ofertes::class)->find($id);

        if (!$oferta) {
            throw $this->createNotFoundException(
                'No existeix la oferta amb id: '.$id
            );
        }

        $entityManager->remove($oferta);
        $entityManager->flush();

        $ofertas = $this->getDoctrine()
            ->getRepository(Ofertes::class)
            ->findByPendientes('2');

        return $this->redirectToRoute('listarOfertasAdmin',
            array("ofertas" => $ofertas,
                "pagina" => "admin")
        );
    }

    /**
     * Función con la cual editamos una oferta concreta
     *
     * @Route("/modificarOferta/{id}", name="modificarOferta")
     */
    public function modificarOferta($id, Request $request){
        $entityManager = $this->getDoctrine()->getManager();
        $oferta = $entityManager->getRepository(Ofertes::class)->find($id);

        if (!$oferta) {
            throw $this->createNotFoundException(
                'No existeix la oferta amb id: '.$id
            );
        }

        $form = $this->createFormBuilder($oferta)
            ->add('Titol', TextType::class, array('attr' => array('class' => 'col-md-6 form-group bmd-label-floating form-control')))
            ->add('Descripcio', TextareaType::class, array('attr' => array('class' => 'col-md-12 form-group bmd-label-floating form-control')))
            ->add('Ubicacio', TextType::class, array('attr' => array('class' => 'col-md-6 form-group bmd-label-floating form-control')))
            ->add('Email', TextType::class, array('attr' => array('class' => 'col-md-6 form-group bmd-label-floating form-control'))) //Indicar en la vista que el mail no es público y que es donde se enviará el CV del usuario inscrito
            ->add('Categoria', EntityType::class, [
                'class' => 'App\Entity\Categoria',
                'choice_label' => 'descripcio'
            ])
            ->add('Inserta', SubmitType::class, ['label' => 'Guardar Oferta'])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {


            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($oferta);
            $entityManager->flush();

            return $this->redirectToRoute('exitoUpdate');
        }

        return $this->render('Empreses/formEmpresas.html.twig', [
            'form' => $form->createView(),
            "pagina" => "admin"
        ]);
    }

    /**
     * @Route("/ofertaActualizada", name="exitoUpdate")
     */
    public function exitoUpdate()
    {
        return $this->render('Empreses/ofertaInsertada.html.twig',
            array("pagina" => "admin",
                "insertUpdate" => "update"
            )
        );
    }

}
