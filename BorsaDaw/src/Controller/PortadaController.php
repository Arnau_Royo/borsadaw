<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class PortadaController extends AbstractController
{

    /**
     * @Route("/home", name="Home")
     */
    public function homePrueba()
    {
        return $this->render('Home/home.html.twig',
            array("pagina" => "inici")
        );
    }
}
