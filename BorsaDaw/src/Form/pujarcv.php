<?php

namespace App\Form;

use App\Entity\Candidat;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class pujarcv extends AbstractType{
  public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('LinkCV', FileType::class, ['label' => 'Inserta el teu CV (PDF file)'])
            ->add('Inserta', SubmitType::class, ['label' => 'Guardar CV'])
            ->getForm()
        ;
    }
/*
    public function configureOptions(OptionsResolver $resolver)
       {
           $resolver->setDefaults(array(
               'data_class' => 'App\Entity\Candidat',
           ));
       }
       */
}
  ?>
