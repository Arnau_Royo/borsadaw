<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Validator\Constraints as Assert;
/**
 * @ORM\Entity(repositoryClass="App\Repository\CandidatRepository")
 */
class Candidat
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $correo;

    /**
      * @ORM\Column(type="string")
      * @Assert\NotBlank(message="Por favor inserta tu currículum")
      * @Assert\File(mimeTypes={"application/pdf"})
      *
      */
    private $linkCV;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $idGoogle;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getCorreo(): ?string
    {
        return $this->correo;
    }

    public function setCorreo(string $correo): self
    {
        $this->correo = $correo;

        return $this;
    }

    public function getLinkCV(): ?string
    {
        return $this->linkCV;
    }

    public function setLinkCV(string $linkCV)
    {
        $this->linkCV = $linkCV;

        return $this;
    }

    public function getIdGoogle(): ?string
    {
        return $this->idGoogle;
    }

    public function setIdGoogle(string $idGoogle): self
    {
        $this->idGoogle = $idGoogle;

        return $this;
    }
}
