<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190326211650 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE candidat DROP password, DROP nombre, DROP primer_apellido, DROP segundo_apellido, DROP telefono, DROP ubicacion, DROP fecha, CHANGE link_cv link_cv VARCHAR(255) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE candidat ADD password VARCHAR(50) NOT NULL COLLATE utf8mb4_unicode_ci, ADD nombre VARCHAR(50) NOT NULL COLLATE utf8mb4_unicode_ci, ADD primer_apellido VARCHAR(50) NOT NULL COLLATE utf8mb4_unicode_ci, ADD segundo_apellido VARCHAR(50) NOT NULL COLLATE utf8mb4_unicode_ci, ADD telefono INT NOT NULL, ADD ubicacion VARCHAR(50) NOT NULL COLLATE utf8mb4_unicode_ci, ADD fecha DATETIME NOT NULL, CHANGE link_cv link_cv VARCHAR(50) NOT NULL COLLATE utf8mb4_unicode_ci');
    }
}
