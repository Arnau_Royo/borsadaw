<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190321115324 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE categoria (id INT AUTO_INCREMENT NOT NULL, descripcio VARCHAR(40) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE candidat (id INT AUTO_INCREMENT NOT NULL, username VARCHAR(50) NOT NULL, password VARCHAR(50) NOT NULL, nombre VARCHAR(50) NOT NULL, primer_apellido VARCHAR(50) NOT NULL, segundo_apellido VARCHAR(50) NOT NULL, telefono INT NOT NULL, correo VARCHAR(100) NOT NULL, ubicacion VARCHAR(50) NOT NULL, fecha DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ofertes (id INT AUTO_INCREMENT NOT NULL, categoria_id INT NOT NULL, titol VARCHAR(50) NOT NULL, descripcio VARCHAR(1000) NOT NULL, data DATETIME NOT NULL, ubicacio VARCHAR(50) NOT NULL, validada INT NOT NULL, email VARCHAR(50) NOT NULL, INDEX IDX_2CA5E0F73397707A (categoria_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE candidat_oferta (id INT AUTO_INCREMENT NOT NULL, ip VARCHAR(50) NOT NULL, username VARCHAR(50) NOT NULL, hora DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE ofertes ADD CONSTRAINT FK_2CA5E0F73397707A FOREIGN KEY (categoria_id) REFERENCES categoria (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE ofertes DROP FOREIGN KEY FK_2CA5E0F73397707A');
        $this->addSql('DROP TABLE categoria');
        $this->addSql('DROP TABLE candidat');
        $this->addSql('DROP TABLE ofertes');
        $this->addSql('DROP TABLE candidat_oferta');
    }
}
