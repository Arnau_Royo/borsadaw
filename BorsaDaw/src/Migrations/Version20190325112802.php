<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190325112802 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE candidat_oferta_ofertes (candidat_oferta_id INT NOT NULL, ofertes_id INT NOT NULL, INDEX IDX_10A05E93AB4E366 (candidat_oferta_id), INDEX IDX_10A05E932E0804C2 (ofertes_id), PRIMARY KEY(candidat_oferta_id, ofertes_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE candidat_oferta_ofertes ADD CONSTRAINT FK_10A05E93AB4E366 FOREIGN KEY (candidat_oferta_id) REFERENCES candidat_oferta (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE candidat_oferta_ofertes ADD CONSTRAINT FK_10A05E932E0804C2 FOREIGN KEY (ofertes_id) REFERENCES ofertes (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE candidat_oferta ADD candidat_id_id INT NOT NULL, ADD candidat_id INT NOT NULL, ADD ofertes_id INT NOT NULL');
        $this->addSql('ALTER TABLE candidat_oferta ADD CONSTRAINT FK_CAB2358EBFA9F225 FOREIGN KEY (candidat_id_id) REFERENCES candidat (id)');
        $this->addSql('ALTER TABLE candidat_oferta ADD CONSTRAINT FK_CAB2358E8D0EB82 FOREIGN KEY (candidat_id) REFERENCES candidat (id)');
        $this->addSql('ALTER TABLE candidat_oferta ADD CONSTRAINT FK_CAB2358E2E0804C2 FOREIGN KEY (ofertes_id) REFERENCES ofertes (id)');
        $this->addSql('CREATE INDEX IDX_CAB2358EBFA9F225 ON candidat_oferta (candidat_id_id)');
        $this->addSql('CREATE INDEX IDX_CAB2358E8D0EB82 ON candidat_oferta (candidat_id)');
        $this->addSql('CREATE INDEX IDX_CAB2358E2E0804C2 ON candidat_oferta (ofertes_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE candidat_oferta_ofertes');
        $this->addSql('ALTER TABLE candidat_oferta DROP FOREIGN KEY FK_CAB2358EBFA9F225');
        $this->addSql('ALTER TABLE candidat_oferta DROP FOREIGN KEY FK_CAB2358E8D0EB82');
        $this->addSql('ALTER TABLE candidat_oferta DROP FOREIGN KEY FK_CAB2358E2E0804C2');
        $this->addSql('DROP INDEX IDX_CAB2358EBFA9F225 ON candidat_oferta');
        $this->addSql('DROP INDEX IDX_CAB2358E8D0EB82 ON candidat_oferta');
        $this->addSql('DROP INDEX IDX_CAB2358E2E0804C2 ON candidat_oferta');
        $this->addSql('ALTER TABLE candidat_oferta DROP candidat_id_id, DROP candidat_id, DROP ofertes_id');
    }
}
