<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190325113746 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE candidat_oferta DROP FOREIGN KEY FK_CAB2358EBFA9F225');
        $this->addSql('DROP INDEX IDX_CAB2358EBFA9F225 ON candidat_oferta');
        $this->addSql('ALTER TABLE candidat_oferta DROP candidat_id_id');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE candidat_oferta ADD candidat_id_id INT NOT NULL');
        $this->addSql('ALTER TABLE candidat_oferta ADD CONSTRAINT FK_CAB2358EBFA9F225 FOREIGN KEY (candidat_id_id) REFERENCES candidat (id)');
        $this->addSql('CREATE INDEX IDX_CAB2358EBFA9F225 ON candidat_oferta (candidat_id_id)');
    }
}
