<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190329112656 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE candidat_oferta DROP FOREIGN KEY FK_CAB2358E2E0804C2');
        $this->addSql('ALTER TABLE candidat_oferta ADD CONSTRAINT FK_CAB2358E2E0804C2 FOREIGN KEY (ofertes_id) REFERENCES ofertes (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE candidat_oferta DROP FOREIGN KEY FK_CAB2358E2E0804C2');
        $this->addSql('ALTER TABLE candidat_oferta ADD CONSTRAINT FK_CAB2358E2E0804C2 FOREIGN KEY (ofertes_id) REFERENCES ofertes (id)');
    }
}
