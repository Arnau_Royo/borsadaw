var app6 = new Vue({
    el: '#app',
    data: {
        listaOfertas: [],
        ofertasInscritas: [],

        user: JSON.parse(sessionStorage.user)
    },
    mounted: function () {
        console.log("Vue ha montado la aplicación de la segunda tabla");
        this.mostrarOfertas();
    },
    methods:
        {
            mostrarOfertas: function () {
                let element = this;
                $.ajax({
                    method: "GET",
                    url: "http://localhost:8000/listarOfertasOrdenadasUsuario",
                })
                    .done(function (res) {
                        element.listaOfertas = res;
                        element.mostrarTodasInscripciones();
                    })
            },
            mostrarTodasInscripciones: function () {
                let element = this;
                $.ajax({
                    method: "GET",
                    url: `http://localhost:8000/listarInscritasUsuario/${element.user.id}`,
                })
                    .done(function (res) {
                        element.ofertasInscritas = res;

                        for (var i = 0; i < element.listaOfertas.length; i++) {
                            for (var j = 0; j < element.ofertasInscritas.length; j++) {
                                if (element.listaOfertas[i].id === element.ofertasInscritas[j].ofertes.id) {
                                    element.listaOfertas[i].validada = "-1"; //Indico que esta oferta el usuario ya esta inscrito
                                }
                            }
                        }
                    })
            }
        }
});

var app = new Vue({
    el: '#app-2',
    data: {
        listaOfertas: [],
        ofertasInscritas: [],

        user: JSON.parse(sessionStorage.user),

    },
    mounted: function () {
        console.log("Vue ha montado la aplicación de la primera tabla");
        this.mostrarOfertas();
    },
    methods:
        {
            mostrarOfertas: function () {
                let element = this;
                $.ajax({
                    method: "GET",
                    url: "http://localhost:8000/listarOfertasOrdenadasUsuario",
                })
                    .done(function (res) {
                        element.listaOfertas = res;
                        element.mostrarTodasInscripciones();
                    })
            },
            mostrarOfertasCategoria: function (categoria) {
                let element = this;
                $.ajax({
                    method: "GET",
                    url: `http://localhost:8000/listarOfertasCategoriaUsuario/${categoria}`,
                })
                    .done(function (res) {
                        element.listaOfertas = res;
                        element.mostrarTodasInscripciones();
                    })
            },
            mostrarTodasOfertas: function () {
                let element = this;
                $.ajax({
                    method: "GET",
                    url: "http://localhost:8000/listarOfertasUsuario",
                })
                    .done(function (res) {
                        element.listaOfertas = res;
                        element.mostrarTodasInscripciones();
                    })
            },
            mostrarTodasInscripciones: function () {
                let element = this;
                $.ajax({
                    method: "GET",
                    url: `http://localhost:8000/listarInscritasUsuario/${element.user.id}`,
                })
                    .done(function (res) {
                        element.ofertasInscritas = res;

                        for (var i = 0; i < element.listaOfertas.length; i++) {
                            for (var j = 0; j < element.ofertasInscritas.length; j++) {
                                if (element.listaOfertas[i].id === element.ofertasInscritas[j].ofertes.id) {
                                    element.listaOfertas[i].validada = "-1"; //Indico que esta oferta el usuario ya esta inscrito
                                }
                            }
                        }
                    })
            },
            inscribirUsuario: function (oferta) {
                let element = this;
                if (`${oferta.validar}` === "true") {
                    axios.post("http://localhost:8000/inscribirOferta", {
                        idGoogle: `${element.user.id}`,
                        idOferta: `${oferta.id}`
                    })
                        .then(function () {
                            Swal.fire({
                                type: 'success',
                                title: 'Inscripció realitzada correctament!',
                                confirmButtonText: "D'acord",
                                confirmButtonColor: '#0F0',
                                text: "S'ha enviat un correu a l'empresa amb les teves dades.",

                            });
                        })

                } else Swal.fire({
                    type: 'warning',
                    title: 'Falten dades!',
                    confirmButtonText: "D'acord",
                    confirmButton: "btn-warning",
                    confirmButtonColor: '#ff9800',
                    text: "Per a poder inscriure't has d'acceptar la politica de protecció de dades",

                });

            }
        }
});