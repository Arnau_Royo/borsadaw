var app = new Vue({
    el: '#appPortada',
    data: {
        contadorASIX: 0,
        contadorDAM: 0,
        contadorDAW: 0,
        contadorSMX: 0,
        listaOfertas: []
    },
    mounted: function () {
        console.log("Vue ha montado la aplicación PORTADA");
        this.mostrarOfertasCategoria("ASIX");
        this.mostrarOfertasCategoria("DAW");
        this.mostrarOfertasCategoria("DAM");
        this.mostrarOfertasCategoria("SMX");
    },
    methods:
        {
            mostrarOfertasCategoria: function (categoria) {
                let element = this;
                $.ajax({
                    method: "GET",
                    url: `http://localhost:8000/listarOfertasCategoriaUsuario/${categoria}`,
                })
                    .done(function (res) {
                        element.listaOfertas = res;
                        if (categoria === "ASIX") element.contadorASIX = element.listaOfertas.length;
                        else if (categoria === "DAM") element.contadorDAM = element.listaOfertas.length;
                        else if (categoria === "DAW") element.contadorDAW = element.listaOfertas.length;
                        else element.contadorSMX = element.listaOfertas.length;
                    })
            }
        }
});