# BorsaDAW

###Integrants
* Fabiana Arzamendia Armoa
* Cristian Ayala Fernandez
* Cristina Ferrer Artigas
* Arnau Royo Raso

###Objectiu 
L'objectiu d'aquest projecte és que els usuaris tinguin una borsa còmode d'ofertes de treball, aquestes estan categoritzades, i aquelles que tenen menys de 15 dies d'antiguitat estan separades en una altre taula per a donar-lis més importància.
Les ofertes són administrades per la persona que faci log-in amb el correu d'admin (pedraljob@gmail.com). I les ofertes són introduïdes per les empreses que així ho vulguin.

[JIRA - CAFC](http://188.166.22.148:8080/secure/RapidBoard.jspa?rapidView=5&projectKey=CAFC)